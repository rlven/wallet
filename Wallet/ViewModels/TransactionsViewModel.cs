﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace Wallet.ViewModels
{
    public class TransactionsViewModel
    {
        public IEnumerable<Transaction> TransactionTo { get; set; }
        public IEnumerable<Transaction> TransactionFrom { get; set; }
    }
}
