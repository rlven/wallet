﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Wallet.Models;

namespace Wallet.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Transaction> Transactions { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>()
                .HasMany(u => u.TransactionsFrom)
                .WithOne(u => u.ApplicationUserFrom)
                .HasForeignKey(u => u.ApplicationUserFromId);

            builder.Entity<ApplicationUser>()
                .HasMany(u => u.TransactionTo)
                .WithOne(u => u.ApplicationUserTo)
                .HasForeignKey(u => u.ApplicationUserToId);
        }

        public DbSet<Wallet.Models.Transaction> Transaction { get; set; }
    }
}
