﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Wallet.Models
{
    public class ApplicationUser : IdentityUser
    {
        public decimal Balance { get; set; }
        public IEnumerable<Transaction> TransactionsFrom { get; set; }
        public IEnumerable<Transaction> TransactionTo { get; set; }
    }
}
