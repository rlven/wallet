﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Wallet.Data;
using Wallet.Models;

namespace Wallet.Controllers
{
    public class TransactionsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IStringLocalizer<MyResources> _localizer;

        public TransactionsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager,
            IStringLocalizer<MyResources> localizer)
        {
            _context = context;
            _userManager = userManager;
            _localizer = localizer;
        }

        // GET: Transactions
        public async Task<IActionResult> Index()
        {
            var currUserId = _userManager.GetUserId(User);

            var currUser = _context.Users.Include(u => u.TransactionTo).Include(u=>u.TransactionsFrom).First(u => u.Id == currUserId);
            
            return View(currUser);
        }

        // GET: Transactions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transaction = await _context.Transaction
                .Include(t => t.ApplicationUserFrom)
                .Include(t => t.ApplicationUserTo)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (transaction == null)
            {
                return NotFound();
            }

            return View(transaction);
        }

        // GET: Transactions/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Transactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Transaction transaction, string userCode)
        {
            var sendTo = _context.Users.FirstOrDefault(u => u.UserName == userCode);
            if (sendTo == null)
            {
                ModelState.AddModelError(string.Empty, _localizer["errMsg2"]);
            }

            var sendFromUserId = _userManager.GetUserId(User);
            var sendFrom = _context.Users.Find(sendFromUserId);
            if (sendFrom.Balance - transaction.TransactionBalance < 0)
            {
                ModelState.AddModelError(string.Empty, _localizer["errMsg1"]);
            }
            if (ModelState.IsValid)
            {
                sendFrom.Balance -= transaction.TransactionBalance;

                sendTo.Balance += transaction.TransactionBalance;

                _context.Update(sendTo);
                _context.Update(sendFrom);
                Transaction newTransaction = new Transaction()
                {
                    TransactionBalance = transaction.TransactionBalance,
                    ApplicationUserFromId = sendFrom.Id,
                    ApplicationUserToId = sendTo.Id,
                    TransactionDate = DateTime.Now
                };
                _context.Add(newTransaction);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(transaction);
        }

        // GET: Transactions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transaction = await _context.Transaction.FindAsync(id);
            if (transaction == null)
            {
                return NotFound();
            }
            ViewData["ApplicationUserFromId"] = new SelectList(_context.Set<ApplicationUser>(), "Id", "Id", transaction.ApplicationUserFromId);
            ViewData["ApplicationUserToId"] = new SelectList(_context.Set<ApplicationUser>(), "Id", "Id", transaction.ApplicationUserToId);
            return View(transaction);
        }

        // POST: Transactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TransactionBalance,ApplicationUserFromId,ApplicationUserToId,Id")] Transaction transaction)
        {
            if (id != transaction.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(transaction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TransactionExists(transaction.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ApplicationUserFromId"] = new SelectList(_context.Set<ApplicationUser>(), "Id", "Id", transaction.ApplicationUserFromId);
            ViewData["ApplicationUserToId"] = new SelectList(_context.Set<ApplicationUser>(), "Id", "Id", transaction.ApplicationUserToId);
            return View(transaction);
        }

        // GET: Transactions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transaction = await _context.Transaction
                .Include(t => t.ApplicationUserFrom)
                .Include(t => t.ApplicationUserTo)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (transaction == null)
            {
                return NotFound();
            }

            return View(transaction);
        }

        // POST: Transactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var transaction = await _context.Transaction.FindAsync(id);
            _context.Transaction.Remove(transaction);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TransactionExists(int id)
        {
            return _context.Transaction.Any(e => e.Id == id);
        }

        public IActionResult TopUpBalance()
        {
            return View();
        }
        public async Task<IActionResult> TopUpBalancePost(string userName, decimal amount)
        {
            var user = _context.Users.FirstOrDefault(u => u.UserName == userName);
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, _localizer["errMsg2"]);
            }

            if (ModelState.IsValid)
            {
                user.Balance += amount;
                _context.Update(user);
                await _context.SaveChangesAsync();
                return Redirect(_localizer["redirect"]);
            }

            return RedirectToAction(nameof(TopUpBalance));
        }
    }
}
