﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Wallet.Models
{
    public class Transaction : Entity
    {
        public decimal TransactionBalance { get; set; }
        public ApplicationUser ApplicationUserFrom { get; set; }
        public string ApplicationUserFromId { get; set; }
        public ApplicationUser ApplicationUserTo { get; set; }
        public string ApplicationUserToId { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}
